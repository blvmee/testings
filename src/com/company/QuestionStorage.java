package com.company;

import java.util.Collections;
import java.util.List;

public class QuestionStorage {

    private List<Question> storage;

    public QuestionStorage() {
        storage = QuestionParser.parseFromTxt();
    }

    public void fillManually(int amount) {
        if (storage == null) {
            storage = QuestionParser.parseFromCommandLine(amount);
        } else {
            storage.addAll(QuestionParser.parseFromCommandLine(amount));
        }
    }

    public int getSize() {
        return storage == null ? 0 : storage.size();
    }

    public Question[] getQuestions(int quantity) throws NotEnoughQuestionsException {
        if (storage == null) throw new NoQuestionsAvailableException("No questions asked!");
        if (quantity > storage.size()) throw new NotEnoughQuestionsException("Not enough questions!");
        Collections.shuffle(storage);
        return storage.subList(0,quantity).toArray(Question[]::new);
    }

}
