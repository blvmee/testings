package com.company;

import java.util.HashMap;

public class Question {

    private final String question;
    private final int value;
    private final String[] options;
    private final boolean[] answers;

    public Question(String question, int value, String[] options, boolean[] answers) {
        this.question = question;
        this.value = value;
        this.options = options;
        this.answers = answers;
    }

    public Question(String question, int value, String[] options, String[] hexedAnswers) {
        this(question, value, options, new boolean[options.length]);
        for (int i = 0; i < options.length; i++) {
            for (String hexedAnswer : hexedAnswers) {
                if (hexedAnswer.equals(Encryptor.encrypt(options[i]))) {
                    answers[i] = true;
                }
            }
        }
    }

    public void printWithAnswers() {
        System.out.println("Q: " + question + " [" + value + " points] ");
        for (int i = 0; i < options.length; i++) {
            System.out.println("\t" + (i + 1) + ": " + options[i] + (answers[i] ? " (+)" : " (-)"));
        }
    }

    public void displayQuestion() {
        System.out.println("Q: " + question + " [" + value + " points] ");
        for (int i = 0; i < options.length; i++) {
            System.out.println("\t" + (i + 1) + ": " + options[i]);
        }
    }

    public int getAmountOfAnswers() {
        int result = 0;
        for (Boolean answer : answers) {
            if (answer) result++;
        }
        return result;
    }

    public int getValue() {
        return value;
    }

    public int checkAnswer(String... answers) {
        int rightAnswers = 0;
        for (String answer : answers) {
            try {
                if (this.answers[Integer.parseInt(answer) - 1]) rightAnswers++;
            } catch (NumberFormatException exception) {
            } // i love scanner
        }
        return rightAnswers * value / getAmountOfAnswers();
    }



}
