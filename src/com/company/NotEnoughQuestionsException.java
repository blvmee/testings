package com.company;

public class NotEnoughQuestionsException extends RuntimeException {

    public NotEnoughQuestionsException(String message) {
        super(message);
    }
}
