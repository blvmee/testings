package com.company;

import java.io.*;
import java.util.*;

public class QuestionParser {

    private static List<String> fails = new ArrayList<>();
    private static final String path = System.getProperty("user.dir") + "\\src\\com\\company\\data.txt";
    static Scanner scan = new Scanner(System.in);

    public static Question parseSingleArg(String arg) throws ArrayIndexOutOfBoundsException, NoSuchElementException, NumberFormatException {
        String[] args = arg.split("_");
        String qText = args[0];
        int qValue = Integer.parseInt(args[1]);
        String[] qOptions = args[2].split(";");
        String[] qAnswersToHex = args[3].split(";");
        return new Question(qText, qValue, qOptions, qAnswersToHex);
    }

    public static ArrayList<Question> parseFromTxt() {
        ArrayList<Question> data = new ArrayList<>();
        try {
            Scanner parser = new Scanner(new FileReader(path));
            while (parser.hasNextLine()) {
                String arg = parser.nextLine();
                try {
                    data.add(parseSingleArg(arg));
                } catch (ArrayIndexOutOfBoundsException | NoSuchElementException | NumberFormatException e) {
                    if (!arg.equals("")) fails.add(arg);
                }
            }
        } catch (FileNotFoundException fnf) {
            fnf.printStackTrace();
            return null;
        }
        if (fails != null) debug();
        return data;
    }

    public static ArrayList<Question> parseFromCommandLine(int amount) {
        ArrayList<Question> data = new ArrayList<>();
        for (int a = 0; a < amount; a++) {
            StringBuilder arg = new StringBuilder();
            System.out.println("Enter question " + (a + 1) + " text:");
            arg.append(scan.nextLine()).append('_');

            System.out.println("How much points will the question cost?");
            arg.append(scan.nextInt()).append('_');

            System.out.println("Enter amount of given options");
            int amountOfOptions = scan.nextInt();
            String[] options = new String[amountOfOptions];
            for (int i = 0; i < options.length ; i++) {
                System.out.print("option " + (i+1) + " = ");
                options[i] = scan.next();
                arg.append(options[i]);
                if (i != options.length - 1) arg.append(';');
            }
            arg.append('_');

            System.out.println("Enter indexes of correct options, each in new line. Enter 0 when you are done");
            boolean[] wasSelectedAsAnAnswer = new boolean[options.length];
            int answerIndex;
            do {
                answerIndex = scan.nextInt();
                if (answerIndex != 0 && !wasSelectedAsAnAnswer[answerIndex - 1]) {
                    arg.append(Encryptor.encrypt(options[answerIndex - 1]));
                    arg.append(';');
                    wasSelectedAsAnAnswer[answerIndex - 1] = true;
                }
            } while (answerIndex != 0);

            writeToFile(arg.toString());
            data.add(parseSingleArg(arg.toString()));
        }
        return data;
    }

    private static void writeToFile(String arg) {
        try {
            FileWriter writer = new FileWriter(path,true);
            writer.write(arg + "\n");
            writer.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private static void debug() {
        if (fails.equals(new ArrayList<>())) return;
        System.out.println("----------DEBUG--------");
        for (String s : fails) {
            System.out.println(s);
        }
        System.out.println("-----------------------");
    }

}
