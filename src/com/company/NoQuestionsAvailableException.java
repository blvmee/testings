package com.company;

public class NoQuestionsAvailableException extends NotEnoughQuestionsException {

    public NoQuestionsAvailableException(String message) {
        super(message);
    }
}
